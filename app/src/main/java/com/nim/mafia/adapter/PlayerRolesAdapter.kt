package com.nim.mafia.adapter

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.nim.mafia.R
import com.nim.mafia.model.Player

class PlayerRolesAdapter(private val context: Context, private var items: ArrayList<Player>) :
    RecyclerView.Adapter<PlayerRolesAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): MyViewHolder {
        val view = inflater.inflate(R.layout.player_names_row, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return this.items.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val player = this.items[position]
        holder.tvName.text = """${(position + 1)}  ${player.name}"""
        holder.ivClose.visibility = View.GONE
        holder.itemView.setOnClickListener {
            AlertDialog.Builder(this.context)
                .setTitle("احراز هویت بازیکن: ${player.name}")
                .setMessage("برای احراز هویت بازیکن، مشاهده نقش را بزنید!")
                .setPositiveButton("مشاهده نقش") { _, _ ->
                    AlertDialog.Builder(this.context)
                        .setTitle("نقش ${player.name} ${if (player.role?.isPositive != false) "مثبت" else "منفی"} است")
                        .setMessage(this.items[position].role?.name)
                        .setPositiveButton(
                            "فهمیدم"
                        ) { ad, _ -> ad.cancel() }
                        .create()
                        .show()
                }
                .setNegativeButton("انصراف") { ad, _ -> ad.cancel() }
                .setCancelable(true)
                .create()
                .show()
        }
    }

    private var inflater: LayoutInflater = LayoutInflater.from(context)

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvName: TextView = itemView.findViewById(R.id.player_name_tv)
        var ivClose: ImageView = itemView.findViewById(R.id.iv_close)
    }
}