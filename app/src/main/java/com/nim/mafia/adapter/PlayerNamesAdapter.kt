package com.nim.mafia.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.nim.mafia.R
import com.nim.mafia.model.Player

class PlayerNamesAdapter(context: Context, private var items: MutableList<Player>) :
    RecyclerView.Adapter<PlayerNamesAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): MyViewHolder {
        val view = inflater.inflate(R.layout.player_names_row, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return this.items.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.tvName.text = this.items[position].name
        holder.ivClose.visibility = View.VISIBLE
        holder.itemView.setOnClickListener {
            items.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    private var inflater: LayoutInflater = LayoutInflater.from(context)

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvName: TextView = itemView.findViewById(R.id.player_name_tv)
        var ivClose: ImageView = itemView.findViewById(R.id.iv_close)
    }
}