package com.nim.mafia.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.nim.mafia.R
import com.nim.mafia.adapter.PlayerRolesAdapter
import com.nim.mafia.model.Player
import kotlinx.android.synthetic.main.activity_result.*

class ResultActivity : AppCompatActivity() {
    private val playerNames: ArrayList<Player> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)
        playerNames.addAll(intent.getParcelableArrayListExtra("players"))
        val adapter = PlayerRolesAdapter(this, playerNames)
        result_rv.adapter = adapter
        result_rv.layoutManager = LinearLayoutManager(applicationContext)
        result_rv.itemAnimator = DefaultItemAnimator()
    }
}
