package com.nim.mafia.activity

import android.content.Intent
import android.os.Bundle
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.nim.mafia.R
import com.nim.mafia.adapter.PlayerNamesAdapter
import com.nim.mafia.model.Player
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private val playerNames: ArrayList<Player> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val adapter = PlayerNamesAdapter(this, playerNames)
        player_names_rv.adapter = adapter
        player_names_rv.layoutManager = LinearLayoutManager(applicationContext)
        player_names_rv.itemAnimator = DefaultItemAnimator()
        player_name_edt.setOnEditorActionListener { v, _, _ ->
            if (v.imeActionId == EditorInfo.IME_ACTION_DONE) {
                submitPlayer(adapter)
            }
            return@setOnEditorActionListener true
        }
        submit_player_name_btn.setOnClickListener {
            submitPlayer(adapter)
        }
        submit_players_btn.setOnClickListener {
            if (playerNames.size > 3) {
                val intent = Intent(this, GameInfoActivity::class.java)
                intent.putParcelableArrayListExtra("players", playerNames)
                startActivity(intent)
            } else {
                Toast.makeText(this, "حداقل تعداد بازیکن باید 4 نفر باشد", Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }

    private fun submitPlayer(adapter: PlayerNamesAdapter) {
        if (player_name_edt.text.isNotEmpty()) {
            if (isDuplicateName(player_name_edt.text.toString()))
                Toast.makeText(this, "این بازیکن قبلاً ثبت شده است", Toast.LENGTH_SHORT).show()
            else {
                playerNames.add(Player(player_name_edt.text.toString(), null))
                adapter.notifyDataSetChanged()
                player_name_edt.setText("")
            }
        } else {
            Toast.makeText(this, "فیلد نام بازیکن نمی تواند خالی باشد", Toast.LENGTH_SHORT)
                .show()
        }
    }

    private fun isDuplicateName(playerName: String): Boolean {
        for (p in playerNames) {
            if (p.name == playerName) return true
        }
        return false
    }
}
