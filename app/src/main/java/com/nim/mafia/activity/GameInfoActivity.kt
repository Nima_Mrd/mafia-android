package com.nim.mafia.activity

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.nim.mafia.R
import com.nim.mafia.model.Player
import com.nim.mafia.model.Role
import kotlinx.android.synthetic.main.activity_game_info.*
import kotlin.random.Random

class GameInfoActivity : AppCompatActivity() {
    private val playerNames: ArrayList<Player> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_info)
        playerNames.addAll(intent.getParcelableArrayListExtra("players"))
        rem_count_tv.text = playerNames.size.toString()
        submit_roles_btn.setOnClickListener {
            val takenPlaces = HashSet<Int>()
            val intent = Intent(this, ResultActivity::class.java)
            var sum = 0
            var tmp = if (mafia_count_edt.text.isNotEmpty()) {
                mafia_count_edt.text.toString().toInt()
            } else {
                0
            }
            sum += tmp
            if (sum > playerNames.size) {
                showError()
                return@setOnClickListener
            }
            for (i in 0 until tmp) {
                var ind = Random.nextInt(playerNames.size)
                while (takenPlaces.contains(ind))
                    ind = Random.nextInt(playerNames.size)
                takenPlaces.add(ind)
                playerNames[ind].role = Role("مافیا", false)
            }
            intent.putExtra("mafia", tmp)
            tmp = if (police_count_edt.text.isNotEmpty()) {
                police_count_edt.text.toString().toInt()
            } else {
                0
            }
            sum += tmp
            if (sum > playerNames.size) {
                showError()
                return@setOnClickListener
            }
            for (i in 0 until tmp) {
                var ind = Random.nextInt(playerNames.size)
                while (takenPlaces.contains(ind))
                    ind = Random.nextInt(playerNames.size)
                takenPlaces.add(ind)
                playerNames[ind].role = Role("شهروند", true)
            }
            intent.putExtra("police", tmp)
            tmp = if (reporter_count_edt.text.isNotEmpty()) {
                reporter_count_edt.text.toString().toInt()
            } else {
                0
            }
            sum += tmp
            if (sum > playerNames.size) {
                showError()
                return@setOnClickListener
            }
            for (i in 0 until tmp) {
                var ind = Random.nextInt(playerNames.size)
                while (takenPlaces.contains(ind))
                    ind = Random.nextInt(playerNames.size)
                takenPlaces.add(ind)
                playerNames[ind].role = Role("خبرنگار", true)
            }
            intent.putExtra("reporter", tmp)
            tmp = if (negotiator_count_edt.text.isNotEmpty()) {
                negotiator_count_edt.text.toString().toInt()
            } else {
                0
            }
            sum += tmp
            if (sum > playerNames.size) {
                showError()
                return@setOnClickListener
            }
            for (i in 0 until tmp) {
                var ind = Random.nextInt(playerNames.size)
                while (takenPlaces.contains(ind))
                    ind = Random.nextInt(playerNames.size)
                takenPlaces.add(ind)
                playerNames[ind].role = Role("مذاکره کننده", false)
            }
            intent.putExtra("negotiator", tmp)
            tmp = if (detective_count_edt.text.isNotEmpty()) {
                detective_count_edt.text.toString().toInt()
            } else {
                0
            }
            sum += tmp
            if (sum > playerNames.size) {
                showError()
                return@setOnClickListener
            }
            for (i in 0 until tmp) {
                var ind = Random.nextInt(playerNames.size)
                while (takenPlaces.contains(ind))
                    ind = Random.nextInt(playerNames.size)
                takenPlaces.add(ind)
                playerNames[ind].role = Role("کارآگاه", true)
            }
            intent.putExtra("detective", tmp)
            tmp = if (sniper_count_edt.text.isNotEmpty()) {
                sniper_count_edt.text.toString().toInt()
            } else {
                0
            }
            sum += tmp
            if (sum > playerNames.size) {
                showError()
                return@setOnClickListener
            }
            for (i in 0 until tmp) {
                var ind = Random.nextInt(playerNames.size)
                while (takenPlaces.contains(ind))
                    ind = Random.nextInt(playerNames.size)
                takenPlaces.add(ind)
                playerNames[ind].role = Role("تک تیرانداز", true)
            }
            intent.putExtra("sniper", tmp)
            tmp = if (doctor_count_edt.text.isNotEmpty()) {
                doctor_count_edt.text.toString().toInt()
            } else {
                0
            }
            sum += tmp
            if (sum > playerNames.size) {
                showError()
                return@setOnClickListener
            }
            for (i in 0 until tmp) {
                var ind = Random.nextInt(playerNames.size)
                while (takenPlaces.contains(ind))
                    ind = Random.nextInt(playerNames.size)
                takenPlaces.add(ind)
                playerNames[ind].role = Role("پزشک", true)
            }
            intent.putExtra("doctor", tmp)
            tmp = if (don_count_edt.text.isNotEmpty()) {
                don_count_edt.text.toString().toInt()
            } else {
                0
            }
            sum += tmp
            if (sum > playerNames.size) {
                showError()
                return@setOnClickListener
            }
            for (i in 0 until tmp) {
                var ind = Random.nextInt(playerNames.size)
                while (takenPlaces.contains(ind))
                    ind = Random.nextInt(playerNames.size)
                takenPlaces.add(ind)
                playerNames[ind].role = Role("رئیس مافیا", false)
            }
            intent.putExtra("don", tmp)
            tmp = if (invulnerable_count_edt.text.isNotEmpty()) {
                invulnerable_count_edt.text.toString().toInt()
            } else {
                0
            }
            sum += tmp
            if (sum != playerNames.size) {
                showError()
                return@setOnClickListener
            }
            for (i in 0 until tmp) {
                var ind = Random.nextInt(playerNames.size)
                while (takenPlaces.contains(ind))
                    ind = Random.nextInt(playerNames.size)
                takenPlaces.add(ind)
                playerNames[ind].role = Role("زره پوش", true)
            }
            intent.putExtra("invulnerable", tmp)
            intent.putParcelableArrayListExtra("players", playerNames)
            startActivity(intent)
        }
    }

    private fun showError() {
        Toast.makeText(this, "تعداد بازیکن ها و نقش ها مساوی نیست", Toast.LENGTH_SHORT).show()
    }
}
