package com.nim.mafia

import android.app.Application
import android.content.Context
import android.os.Build
import java.util.*


class App : Application() {

    override fun onCreate() {
        super.onCreate()
        appContext = applicationContext
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            val configuration = resources.configuration
            configuration.setLayoutDirection(Locale("fa"))
            resources.updateConfiguration(configuration, resources.displayMetrics)
        }
    }

    companion object {
        lateinit var appContext: Context
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
    }
}
