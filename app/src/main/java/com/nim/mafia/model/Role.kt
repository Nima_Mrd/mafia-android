package com.nim.mafia.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Role(var name: String, var isPositive: Boolean?) : Parcelable