package com.nim.mafia.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Player(var name: String, var role: Role?) : Parcelable